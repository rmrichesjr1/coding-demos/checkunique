// checkunique.go

// This quick program generates or checks a stream of non-repeating data to
// verify block device capacity and integrity.  When checking, information
// about the location and content of mismatches is printed.
//
// Syntax:
//
//     checkunique mode prefix [file]
//
// The 'mode' argument should be either "write" or "check".
//
// The 'prefix' argument is a one-character string so that separate partitions
// can be done without losing uniqueness across partitions.
//
// The optional 'file' argument is where output will be written or whether
// data will be read when checking.  If that argument is omitted, stdout or
// stdin will be used.
//
// The data will consist of the prefix character, a decimal representation of
// the offset to which this line's prefix character will be written or is
// expected to be read, and a newline.  The newline helps the file be viewable
// in a reasonable manner as text.
//
// Serving suggestion to do multiple runs in parallel:
//
//     parallel -j3 ./checkunique write ::: {A,B,C} :::+ /dev/sdx{1,2,3}

package main

import (
  "bytes"
  "fmt"
  "io"
  "log"
  "os"
  "strconv"
  "time"
)

//////////////////////////////////////////////////////////////////////////////

// Constant:

// Number of digits in offset.  This lets the program handle up to 99.999TB.
const digits = 14

// Value to check whether digits above is correct:
const checkDigitsValue = 14 * 1000 * 1000 * 1000 * 1000

// Total line length, including prefix character and newline:
const lineLength = 1 + digits + 1

// Valid mode string for writing:
const modeWrite = "write"

// Valid mode string for checking:
const modeCheck = "check"

//////////////////////////////////////////////////////////////////////////////

// Message printing utility functions:

// Set this to non-zero to enable debug printing:
const enableDebugPrint = 0
// const enableDebugPrint = 1

func printDebug(msg string) {
  if (0 == enableDebugPrint) {
    return
  }
  fmt.Printf( "DEBUG:     %s\n", msg)
}

func printInfo(msg string) {
  fmt.Printf( "     info: %s\n", msg)
}

func printWarning(msg string) {
  fmt.Printf( "  Warning: %s\n", msg)
}

// This function takes a string.
// This function does not return:
func printError(msg string) {
  fmt.Printf( "*** ERROR: %s\n", msg)
  os.Exit(1)
}

// This function takes an error.
// This function does not return:
func printFatal(err error) {
  log.Fatal(err)
  os.Exit(1)
}

// This function does not return:
func printUsageError(msg string) {
  fmt.Printf( "*** ERROR: %s\n\n", msg)
  printUsage()
  os.Exit(1)
}

// Print the usage message and return:
func printUsage() {
  fmt.Printf("usage: %s mode prefix [file]\n", os.Args[0])
  fmt.Println("")
  fmt.Println("    mode:")
  fmt.Println("        write")
  fmt.Println("        check")
  fmt.Println("")
  fmt.Println("    prefix:")
  fmt.Println("        one character, any character")
  fmt.Println("")
}

//////////////////////////////////////////////////////////////////////////////

// Check assumptions.  Error output if there's a problem.
func checkAssumptions() {
  leng := len(strconv.Itoa(checkDigitsValue))
  if (digits != leng) {
    printError( fmt.Sprintf( "Internal assumption invalid: %d vs. %d",
                digits, leng))
    // That function does no return.
  }
}

//////////////////////////////////////////////////////////////////////////////

// Return the mode, the prefix character (as a string), the file name (or
// "stdout" or "stdin"), and a File object.
func getArgs() ( string, string, string, * os.File) {
  // Default to use stdin and stdout.
  inputName := "(stdin)"
  outputName := "(stdout)"
  theInput  := os.Stdin
  theOutput := os.Stdout
  var err error = nil
  //
  leng := len(os.Args)
  if (4 < leng) {
    // Too many arguments; error out.
    printUsageError("Too many arguments.")
    // That function does not return.
  }
  if (3 > leng) {
    // Too few arguments; error out.
    printUsageError("Too few arguments.")
    // That function does not return.
  }
  //
  mode   := os.Args[1]
  prefix := os.Args[2]
  if ((modeWrite != mode) && (modeCheck != mode)) {
    // Invalid mode argument; error out.
    printUsageError(fmt.Sprintf("Invalid mode argument: " + mode))
    // That function does not return.
  }
  if (1 != len(prefix)) {
    // Invalid prefix length; error out.
    printUsageError(fmt.Sprintf("Invalid prefix length: " + mode))
    // That function does not return.
  }
  //
  var theFilename string
  var theFileObj * os.File
  if (4 == leng) {
    // Named output file:
    theFilename = os.Args[3]
    var direction string
    if (modeWrite == mode) {
      direction = "output"
      theFileObj, err = os.Create(theFilename)
    } else {
      direction = "input"
      theFileObj, err = os.Open(theFilename)
    }
    if (nil != err) {
      printError("Failed to open " + direction + " file: " + theFilename)
      // That function does not return.
    }
  } else {
    if (modeWrite == mode) {
      // Default to stdout:
      theFilename = outputName
      theFileObj  = theOutput
    } else {
      // Default to stdin:
      theFilename = inputName
      theFileObj  = theInput
    }
  }
  return mode, prefix, theFilename, theFileObj
}

//////////////////////////////////////////////////////////////////////////////

// Produce a slice of strings 256 long, a lookup table for printing.
// Maximum string length is 3.

func makePrintTable() [] string {
  result := make( [] string, 256)

  // ASCII:

  // Control characters:
  result[0x00] = "NUL"
  result[0x01] = "^a"
  result[0x02] = "^b"
  result[0x03] = "^c"
  result[0x04] = "^d"
  result[0x05] = "^e"
  result[0x06] = "^f"
  result[0x07] = "^g"
  result[0x08] = "^h"
  result[0x09] = "^i"
  result[0x0a] = "^j"
  result[0x0b] = "^k"
  result[0x0c] = "^l"
  result[0x0d] = "^m"
  result[0x0e] = "^n"
  result[0x0f] = "^o"

  result[0x10] = "^p"
  result[0x11] = "^q"
  result[0x12] = "^r"
  result[0x13] = "^s"
  result[0x14] = "^t"
  result[0x15] = "^u"
  result[0x16] = "^v"
  result[0x17] = "^w"
  result[0x18] = "^x"
  result[0x19] = "^y"
  result[0x1a] = "^z"
  result[0x1b] = "ESC"
  result[0x1c] = "FS"
  result[0x1d] = "GS"
  result[0x1e] = "RS"
  result[0x1f] = "US"

  // Punctuation and digits:
  result[0x20] = " "
  result[0x21] = "!"
  result[0x22] = "\""
  result[0x23] = "#"
  result[0x24] = "$"
  result[0x25] = "%"
  result[0x26] = "&"
  result[0x27] = "'"
  result[0x28] = "("
  result[0x29] = ")"
  result[0x2a] = "*"
  result[0x2b] = "+"
  result[0x2c] = ","
  result[0x2d] = "-"
  result[0x2e] = "."
  result[0x2f] = "/"

  result[0x30] = "0"
  result[0x31] = "1"
  result[0x32] = "2"
  result[0x33] = "3"
  result[0x34] = "4"
  result[0x35] = "5"
  result[0x36] = "6"
  result[0x37] = "7"
  result[0x38] = "8"
  result[0x39] = "9"
  result[0x3a] = ":"
  result[0x3b] = ";"
  result[0x3c] = "<"
  result[0x3d] = "="
  result[0x3e] = ">"
  result[0x3f] = "?"

  // Uppercase letters:
  result[0x40] = "@"
  result[0x41] = "A"
  result[0x42] = "B"
  result[0x43] = "C"
  result[0x44] = "D"
  result[0x45] = "E"
  result[0x46] = "F"
  result[0x47] = "G"
  result[0x48] = "H"
  result[0x49] = "I"
  result[0x4a] = "J"
  result[0x4b] = "K"
  result[0x4c] = "L"
  result[0x4d] = "M"
  result[0x4e] = "N"
  result[0x4f] = "O"

  result[0x50] = "P"
  result[0x51] = "Q"
  result[0x52] = "R"
  result[0x53] = "S"
  result[0x54] = "T"
  result[0x55] = "U"
  result[0x56] = "V"
  result[0x57] = "W"
  result[0x58] = "X"
  result[0x59] = "Y"
  result[0x5a] = "Z"
  result[0x5b] = "["
  result[0x5c] = "\\"
  result[0x5d] = "]"
  result[0x5e] = "^"
  result[0x5f] = "_"

  // Lowercase letters:
  result[0x60] = "`"
  result[0x61] = "a"
  result[0x62] = "b"
  result[0x63] = "c"
  result[0x64] = "d"
  result[0x65] = "e"
  result[0x66] = "f"
  result[0x67] = "g"
  result[0x68] = "h"
  result[0x69] = "i"
  result[0x6a] = "j"
  result[0x6b] = "k"
  result[0x6c] = "l"
  result[0x6d] = "m"
  result[0x6e] = "n"
  result[0x6f] = "o"

  result[0x70] = "p"
  result[0x71] = "q"
  result[0x72] = "r"
  result[0x73] = "s"
  result[0x74] = "t"
  result[0x75] = "u"
  result[0x76] = "v"
  result[0x77] = "w"
  result[0x78] = "x"
  result[0x79] = "y"
  result[0x7a] = "z"
  result[0x7b] = "{"
  result[0x7c] = "|"
  result[0x7d] = "}"
  result[0x7e] = "~"
  result[0x7f] = "DEL"

  // Above ASCII:
  for i := 128; 256 > i; i++ {
    result[i] = "M-?"
  }

  return(result)
}

//////////////////////////////////////////////////////////////////////////////

// Return one line of text.

func makeLine( theOffset uint64, theFormat string) [] byte {
  return([] byte(fmt.Sprintf( theFormat, theOffset)))
}

//////////////////////////////////////////////////////////////////////////////

// Check the length of one test line.  On error, do _NOT_ return.

func checkLength(theFormat string) {
  theLine := makeLine( checkDigitsValue, theFormat)
  leng := len(theLine)
  if (lineLength != leng) {
    printDebug(fmt.Sprintf( "checkLength: line got: %s", theLine))
    printError(fmt.Sprintf( "Line length check failed: wanted %d, got %d",
                            lineLength, leng))
    // That function does not return.
  }
}

//////////////////////////////////////////////////////////////////////////////

// Generate the format string: "A%014d\n"

func makeFormat(prefix string) string {
  percent := "%"
  digitCountString := fmt.Sprintf( "%d", digits)
  newline := "\n"
  theFormat := fmt.Sprintf( "%s%s0%sd%s", prefix, percent,
                            digitCountString,  newline)
  printDebug(fmt.Sprintf( "makeFormat: format: %s", theFormat))
  checkLength(theFormat)
  return(theFormat)
}

//////////////////////////////////////////////////////////////////////////////

func doWrite( thePrefix string, theFilename string, theFileObj * os.File,
              theFormat string, printTable [] string) uint64 {
  printDebug("doWrite starting:")
  // 
  // Initial state:
  var bytesDone uint64 = 0
  //
  // Loop:
  for (true) {
    _, err := fmt.Fprint( theFileObj, string(makeLine( bytesDone, theFormat)))
    if (nil != err) {

// FIXME: Handle some errors more gracefully and others.

      log.Printf( "doWrite() error: %v", err)
    }
    bytesDone += lineLength
  }
  printDebug("doWrite finished:" + strconv.FormatUint( bytesDone, 10))
  return(bytesDone)
}

//////////////////////////////////////////////////////////////////////////////

func printMismatch(msg string) {
  fmt.Println(msg)
}

//////////////////////////////////////////////////////////////////////////////

// Print one side of a failed comparison:

func printOne( prefix string, theData [] byte, printTable [] string) {
  theHex := ""
  thePrinted := ""
  for i := 0; lineLength > i; i++ {
    theByte := theData[i]
    // Four characters per byte:
    theHex     += fmt.Sprintf( "  %02x", theByte)
    thePrinted += fmt.Sprintf( " %3s", printTable[theByte])
  }
  printMismatch(fmt.Sprintf( "%s    %s", prefix, theHex))
  printMismatch(fmt.Sprintf( "%s    %s", prefix, thePrinted))
}

//////////////////////////////////////////////////////////////////////////////

// Return the number of bytes processed and the number of rows printed.

func doCheck( thePrefix string, theFilename string, theFileObj * os.File,
              theFormat string, printTable [] string) (uint64, int) {
  printDebug("doCheck starting:")
  // 
  // Initial state:
  var bytesDone uint64 = 0
  var rowsPrinted int = 0
  theBuffer := make( [] byte, lineLength)
  //
  // Loop once per line from the file:
  for (true) {
    n, err := theFileObj.Read(theBuffer)
    if (nil != err) {
      if (io.EOF == err) {
        printDebug("hit end of input file")
        break
      }
      printFatal(err)
    }
    if (lineLength > n) {
      // Did not get enough the first time, so must try again until enough
      // bytes have been read.  For performance with big files, hope this does
      // not happen too very often.
      got := n
      for (lineLength > got) {
        otherBuffer := make( [] byte, (lineLength - got))
        n2, err := theFileObj.Read(otherBuffer)
        if (nil != err) {
          if (io.EOF == err) {
            printDebug("hit end of input file")
            break
          }
          printFatal(err)
        }
        theBuffer = append( theBuffer, otherBuffer...)
        got += n2
      }
    }
    // Paranoia check:
    if (lineLength != n) {
      printError(fmt.Sprintf( "doCheck got invalid length: %d", n))
      // That function does not return.
    }
    printDebug(fmt.Sprintf( "Read %d bytes at offset %d.", n, bytesDone))
    expected := makeLine( bytesDone, theFormat)
    if (0 != bytes.Compare( expected, theBuffer)) {
      // A mismatch, must print something.
      printMismatch(fmt.Sprintf( "Mismatch at offset %d, 0x%x",
                                 bytesDone, bytesDone))
      printOne( "exp", expected, printTable)
      printOne( "got", theBuffer, printTable)
      rowsPrinted += 1
    }
    bytesDone += uint64(n)
  }
  printDebug("doCheck finished:" + fmt.Sprintf( "%d", bytesDone))
  return bytesDone, rowsPrinted
}

//////////////////////////////////////////////////////////////////////////////

func doWork( theMode string, thePrefix string,
             theFilename string, theFileObj * os.File,
             printTable [] string) {
  printDebug("doWork starting:")
  theFormat := makeFormat(thePrefix)
  startTime := time.Now()
  var bytesDone uint64
  var rowsPrinted int
  if (modeWrite == theMode) {
    bytesDone = doWrite( thePrefix, theFilename, theFileObj,
                         theFormat, printTable)
  } else {
    bytesDone, rowsPrinted = doCheck( thePrefix, theFilename, theFileObj,
                                      theFormat, printTable)
  }
  //
  // Prepare to print summary statistics:
  miBytes := float32(bytesDone) / 1024.0 / 1024.0
  giBytes := miBytes / 1024.0
  endTime := time.Now()
  elapsed := float32(endTime.Sub(startTime)) / 1000.0 / 1000.0 / 1000.0
  miBps := miBytes / elapsed
  stats := []string{}
  stats = append( stats, "")
  stats = append( stats, fmt.Sprintf( "           File: %s",   theFilename))
  stats = append( stats, fmt.Sprintf( "Bytes processed: %d",   bytesDone))
  stats = append( stats, fmt.Sprintf( "  MiB processed: %.2f", miBytes))
  stats = append( stats, fmt.Sprintf( "  GiB processed: %.2f", giBytes))
  if (modeWrite != theMode) {
    stats = append( stats, fmt.Sprintf( "   Rows printed: %d",   rowsPrinted))
  }
  stats = append( stats, fmt.Sprintf( "   elapsed secs: %.2f", elapsed))
  stats = append( stats, fmt.Sprintf( " MiB per second: %.2f", miBps))
  stats = append( stats, "")
  //
  // Print summary statistics:
  for _, s := range stats {
    printInfo(s)
  }
  printDebug("doWork finished.")
}

//////////////////////////////////////////////////////////////////////////////

func closeFile(theFile * os.File) {
  printDebug("closeFile starting:")
  if ((os.Stdout != theFile) && (os.Stdin != theFile)) {
    // Doesn't matter here whether it is output or input:
    defer theFile.Close()
  }
  printDebug("closeFile finished.")
}

//////////////////////////////////////////////////////////////////////////////

// Start here:
func main() {
  checkAssumptions()
  mode, prefix, filename, fileObj := getArgs()
  doWork( mode, prefix, filename, fileObj, makePrintTable())
  closeFile(fileObj)
}

//////////////////////////////////////////////////////////////////////////////

// The End.
