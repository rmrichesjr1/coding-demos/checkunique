# Makefile

# This is the Makefile for checkunique.  It builds only the C version.  The
# Golang version can be built with "go build checkunique.go".

CC = gcc

CCFLAGS = -Wall -O

SRC  = checkunique.c

DOTO = checkunique.o

BIN  = checkunique

all: $(BIN)

$(DOTO): $(SRC)
	$(CC) $(CCFLAGS) -c $(SRC)

$(BIN): $(DOTO)
	$(CC) $(CCFLAGS) -o $(BIN) $(DOTO)

clean:
	- \rm -f $(DOTO) $(BIN)

# The End.
