// checkunique.c

// This is the source for the C version of checkunique.

// This quick program generates or checks a stream of non-repeating data to
// verify block device capacity and integrity.  When checking, information
// about the location and content of mismatches is printed.
//
// Syntax:
//
//     checkunique mode prefix [file]
//
// The 'mode' argument should be either "write" or "check".
//
// The 'prefix' argument is a one-character string so that separate partitions
// can be done without losing uniqueness across partitions.
//
// The optional 'file' argument is where output will be written or whether
// data will be read when checking.  If that argument is omitted, stdout or
// stdin will be used.
//
// The data will consist of the prefix character, a decimal representation of
// the offset to which this line's prefix character will be written or is
// expected to be read, and a newline.  The newline helps the file be viewable
// in a reasonable manner as text.
//
// Serving suggestion to do multiple runs in parallel:
//
//     parallel -j3 ./checkunique write ::: {A,B,C} :::+ /dev/sdx{1,2,3}

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

//////////////////////////////////////////////////////////////////////////////

// Constants:

// Yes, C convention is to use all uppercase for constants.  Deliberate
// decision made to violate that convention to ease porting from Go to C.

// Number of digits in offset.  This lets the program handle up to 99.999TB.
#define digits   14

// Size of temporary buffer:
#define tempBufSize  65536

// Size of big I/O buffer:
#define bigBufSize (1024 * 1024)

// Value to check whether digits above is correct:
#define checkDigitsValue (14ll * 1000ll * 1000ll * 1000ll * 1000ll)

// Total line length, including prefix character and newline:
#define lineLength (1 + digits + 1)

// Padding for I/O buffer and line buffer:
#define bufferPad 4096

// Format string length plus some margin:
#define formatLength 32

// Length of buffer for digit count:
#define countLength 12

// Valid mode string for writing:
#define modeWrite "write"

// Valid mode string for checking:
#define modeCheck "check"

// Mismatch print buffer size:
#define mismatchPrintBufSize 128

//////////////////////////////////////////////////////////////////////////////

// Temporary buffer for quick string checks, error printing, etc.
char tempBuf[tempBufSize];

// Big buffer for I/O:
char bigBuf[bigBufSize + bufferPad];

// Format string buffer:
char formatBuf[formatLength];

// Mismatch print buffers:
char mismatchHexBuf[mismatchPrintBufSize];
char mismatchPrintedBuf[mismatchPrintBufSize];

// Line buffer for checking:
char lineBuf[lineLength + bufferPad];

//////////////////////////////////////////////////////////////////////////////

// Message printing utility functions:

// Set this to non-zero to enable debug printing:
#define enableDebugPrint  0
// #define enableDebugPrint  1

void printDebug(const char * msg) {
  if (0 == enableDebugPrint) {
    return;
  }
  printf( "DEBUG:     %s\n", msg);
}

void printInfo(const char * msg) {
  printf( "     info: %s\n", msg);
}

void printWarning(const char * msg) {
  printf( "  Warning: %s\n", msg);
}

// This function takes a string.
// This function does not return:
void printError(const char * msg) {
  printf( "*** ERROR: %s\n", msg);
  exit(1);
}

// No need for printFatal(), because C does not have an 'error' type.

// Still dealing with 1970-era limitations:
void printUsage();

// This function does not return:
void printUsageError(const char * msg) {
  printf( "*** ERROR: %s\n\n", msg);
  printUsage();
  exit(1);
}

// Print the usage message and return:
void printUsage() {
  printf("usage: checkunique mode prefix [file]\n");
  printf("\n");
  printf("    mode:\n");
  printf("        write\n");
  printf("        check\n");
  printf("\n");
  printf("    prefix:\n");
  printf("        one character, any character\n");
  printf("\n");
}

//////////////////////////////////////////////////////////////////////////////

// Check assumptions.  Error output if there's a problem.
void checkAssumptions() {
  snprintf( tempBuf, tempBufSize, "%llu", checkDigitsValue);
  int leng = strlen(tempBuf);
  if (digits != leng) {
    snprintf( tempBuf, tempBufSize,
              "Internal assumption invalid: %d vs. %d", digits, leng);
    printError(tempBuf);
    // That function does no return.
  }
}

//////////////////////////////////////////////////////////////////////////////

// Return the mode, the prefix character (as a string), the file name (or
// "stdout" or "stdin"), and a File object.
void getArgs( int argc, const char ** argv,
              const char ** modeP, const char ** prefixP,
              const char ** filenameP, int * fdP) {
  // Default to use stdin and stdout.
  const char *  inputName = "(stdin)";
  const char * outputName = "(stdout)";
  int theInput  =  STDIN_FILENO;
  int theOutput = STDOUT_FILENO;
  //
  if (4 < argc) {
    // Too many arguments; error out.
    printUsageError("Too many arguments.");
    // That function does not return.
  }
  if (3 > argc) {
    // Too few arguments; error out.
    printUsageError("Too few arguments.");
    // That function does not return.
  }
  //
  *modeP   = argv[1];
  *prefixP = argv[2];
  if (   (0 != strcmp( modeWrite, *modeP))
      && (0 != strcmp( modeCheck, *modeP))) {
    // Invalid mode argument; error out.
    snprintf( tempBuf, tempBufSize, "Invalid mode argument: %s", *modeP);
    printUsageError(tempBuf);
    // That function does not return.
  }
  if (1 != strlen(*prefixP)) {
    // Invalid prefix length; error out.
    snprintf( tempBuf, tempBufSize, "Invalid prefix length: %s", *modeP);
    printUsageError(tempBuf);
    // That function does not return.
  }
  //
  if (4 == argc) {
    const char * direction;
    int flags;
    // Named output file:
    *filenameP = argv[3];
    if (0 == strcmp( modeWrite, *modeP)) {
      direction = "output";
      flags = O_CREAT | O_TRUNC | O_WRONLY;
    } else {
      direction = "input";
      flags = O_RDONLY;
    }
    if (0 > (*fdP = open( *filenameP, flags))) {
      snprintf( tempBuf, tempBufSize,
                "Failed to open %s file: %s", direction, *filenameP);
      printError(tempBuf);
      // That function does not return.
    }
  } else {
    if (0 == strcmp( modeWrite, *modeP)) {
      // Default to stdout:
      *filenameP = outputName;
      *fdP       = theOutput;
    } else {
      // Default to stdin:
      *filenameP = inputName;
      *fdP       = theInput;
    }
  }
}

//////////////////////////////////////////////////////////////////////////////

// Produce a slice of strings 256 long, a lookup table for printing.
// Maximum string length is 3.

const char ** makePrintTable() {
  const char ** result = (const char **) malloc(256 * sizeof(char *));
  if (NULL == result) {
    printError("Failed to malloc print table.");
    // That function does not return.
  }

  // ASCII:

  // Control characters:
  result[0x00] = "NUL";
  result[0x01] = "^a";
  result[0x02] = "^b";
  result[0x03] = "^c";
  result[0x04] = "^d";
  result[0x05] = "^e";
  result[0x06] = "^f";
  result[0x07] = "^g";
  result[0x08] = "^h";
  result[0x09] = "^i";
  result[0x0a] = "^j";
  result[0x0b] = "^k";
  result[0x0c] = "^l";
  result[0x0d] = "^m";
  result[0x0e] = "^n";
  result[0x0f] = "^o";

  result[0x10] = "^p";
  result[0x11] = "^q";
  result[0x12] = "^r";
  result[0x13] = "^s";
  result[0x14] = "^t";
  result[0x15] = "^u";
  result[0x16] = "^v";
  result[0x17] = "^w";
  result[0x18] = "^x";
  result[0x19] = "^y";
  result[0x1a] = "^z";
  result[0x1b] = "ESC";
  result[0x1c] = "FS";
  result[0x1d] = "GS";
  result[0x1e] = "RS";
  result[0x1f] = "US";

  // Punctuation and digits:
  result[0x20] = " ";
  result[0x21] = "!";
  result[0x22] = "\"";
  result[0x23] = "#";
  result[0x24] = "$";
  result[0x25] = "%";
  result[0x26] = "&";
  result[0x27] = "'";
  result[0x28] = "(";
  result[0x29] = ")";
  result[0x2a] = "*";
  result[0x2b] = "+";
  result[0x2c] = ",";
  result[0x2d] = "-";
  result[0x2e] = ".";
  result[0x2f] = "/";

  result[0x30] = "0";
  result[0x31] = "1";
  result[0x32] = "2";
  result[0x33] = "3";
  result[0x34] = "4";
  result[0x35] = "5";
  result[0x36] = "6";
  result[0x37] = "7";
  result[0x38] = "8";
  result[0x39] = "9";
  result[0x3a] = ":";
  result[0x3b] = ";";
  result[0x3c] = "<";
  result[0x3d] = "=";
  result[0x3e] = ">";
  result[0x3f] = "?";

  // Uppercase letters:
  result[0x40] = "@";
  result[0x41] = "A";
  result[0x42] = "B";
  result[0x43] = "C";
  result[0x44] = "D";
  result[0x45] = "E";
  result[0x46] = "F";
  result[0x47] = "G";
  result[0x48] = "H";
  result[0x49] = "I";
  result[0x4a] = "J";
  result[0x4b] = "K";
  result[0x4c] = "L";
  result[0x4d] = "M";
  result[0x4e] = "N";
  result[0x4f] = "O";

  result[0x50] = "P";
  result[0x51] = "Q";
  result[0x52] = "R";
  result[0x53] = "S";
  result[0x54] = "T";
  result[0x55] = "U";
  result[0x56] = "V";
  result[0x57] = "W";
  result[0x58] = "X";
  result[0x59] = "Y";
  result[0x5a] = "Z";
  result[0x5b] = "[";
  result[0x5c] = "\\";
  result[0x5d] = "]";
  result[0x5e] = "^";
  result[0x5f] = "_";

  // Lowercase letters:
  result[0x60] = "`";
  result[0x61] = "a";
  result[0x62] = "b";
  result[0x63] = "c";
  result[0x64] = "d";
  result[0x65] = "e";
  result[0x66] = "f";
  result[0x67] = "g";
  result[0x68] = "h";
  result[0x69] = "i";
  result[0x6a] = "j";
  result[0x6b] = "k";
  result[0x6c] = "l";
  result[0x6d] = "m";
  result[0x6e] = "n";
  result[0x6f] = "o";

  result[0x70] = "p";
  result[0x71] = "q";
  result[0x72] = "r";
  result[0x73] = "s";
  result[0x74] = "t";
  result[0x75] = "u";
  result[0x76] = "v";
  result[0x77] = "w";
  result[0x78] = "x";
  result[0x79] = "y";
  result[0x7a] = "z";
  result[0x7b] = "{";
  result[0x7c] = "|";
  result[0x7d] = "}";
  result[0x7e] = "~";
  result[0x7f] = "DEL";

  // Above ASCII:
  for ( int i = 128; 256 > i; i++) {
    result[i] = "M-?";
  }

  return(result);
}

//////////////////////////////////////////////////////////////////////////////

// Return one line of text.
//
// NOTE: A trailing null/zero byte is produced.
//
// The snprintf() man page is not entirely clear of what happens if there is
// not enough room for the null/zero byte.  So, play it safe by extending or
// padding the size of the buffer.

void makeLine( char * destP, unsigned long long theOffset,
               const char * theFormat) {
  int wrote = snprintf( destP, lineLength + 1, theFormat, theOffset);
  if (lineLength != wrote) {
    snprintf( tempBuf, tempBufSize,
              "snprintf() returned %d; expected %d", wrote, lineLength);
    printError(tempBuf);
    // That function does not return.
  }
}

//////////////////////////////////////////////////////////////////////////////

// Check the length of one test line.  On error, do _NOT_ return.

void checkLength(const char * theFormat) {
  char checkBuf[tempBufSize];
  // Ensure there will be a null byte to allow strlen():
  memset( checkBuf, 0, lineLength * 2);
  // Now, write a string:
  makeLine( checkBuf, checkDigitsValue, theFormat);
  int leng = strlen(checkBuf);
  if (lineLength != leng) {
    snprintf( tempBuf, tempBufSize,
              "checkLength: line got: %s", checkBuf);
    printDebug(tempBuf);
    snprintf( tempBuf, tempBufSize,
              "Line length check failed: wanted %d, got %d",
              lineLength, leng);
    printError(tempBuf);
    // That function does not return.
  }
}

//////////////////////////////////////////////////////////////////////////////

// Generate the format string: "A%014llx\n"

void makeFormat(const char * prefix) {
  const char * percent = "%";
  char countBuf[countLength];
  snprintf( countBuf, countLength, "%d", digits);
  const char * newline = "\n";
  snprintf( formatBuf, formatLength,
            "%s%s0%sllx%s", prefix, percent, countBuf, newline);
  snprintf( tempBuf, tempBufSize, "makeFormat: format: %s", formatBuf);
  printDebug(tempBuf);
  checkLength(formatBuf);
}

//////////////////////////////////////////////////////////////////////////////

unsigned long long doWrite( const char * theFilename, int theFileFd,
                            const char * theFormat,
                            const char ** printTable) {
  printDebug("doWrite starting:");
  // 
  // Initial state:
  unsigned long long bytesDone = 0;
  //
  // Nested loops:
  int keepgoing = 1;
  while (0 != keepgoing) {
    // Loop once per big buffer:
    //
    // Fill the buffer:
    char * bigP  = bigBuf;
    char * pastP = bigP + bigBufSize;
    while (pastP > bigP) {
      makeLine( bigP, bytesDone, theFormat);
      bigP      += lineLength;
      bytesDone += lineLength;
    }
    // Write out the buffer:
    bigP = bigBuf;
    int todo = bigBufSize;
    while ((0 != keepgoing) && (pastP > bigP)) {
      ssize_t did = write( theFileFd, bigP, todo);
      if (0 > did) {
        // An error occurred.
        int gotErr = errno;
        char * strErr = strerror(gotErr);
// TODO : Treat some errors more equally than other errors.
        //
        snprintf( tempBuf, tempBufSize,
                  "doWrite hit errno %d, %s", gotErr, strErr);
        printDebug(tempBuf);
        fflush(stdout);
        fprintf( stderr, "%s\n", tempBuf);
        fflush(stderr);
        keepgoing = 0;
        break; // break out of all loops
      }
      bigP += did;
      todo -= did;
    }
  }
  snprintf( tempBuf, tempBufSize, "doWrite finished: %llu", bytesDone);
  printDebug(tempBuf);
  return(bytesDone);
}

//////////////////////////////////////////////////////////////////////////////

void printMismatch(const char * msg) {
  printf( "%s\n", msg);
}

//////////////////////////////////////////////////////////////////////////////

// Print one side of a failed comparison:

void printOne( const char * prefix, const char * theData,
               const char ** printTable) {
  char *     hexP = mismatchHexBuf;
  char * printedP = mismatchPrintedBuf;
  for ( int i = 0; lineLength > i; i++) {
    char theByte = theData[i];
    // Four characters per byte:
    snprintf( hexP,
              (mismatchPrintBufSize - (hexP - mismatchHexBuf)),
              "  %02x", theByte);
    snprintf( printedP,
              (mismatchPrintBufSize - (printedP - mismatchPrintedBuf)),
              " %3s", printTable[(int)theByte]);
  }
  printf( "%s    %s", prefix, mismatchHexBuf);
  printf( "%s    %s", prefix, mismatchPrintedBuf);
}

//////////////////////////////////////////////////////////////////////////////

// Return the number of bytes processed and the number of rows printed.

void doCheck( const char * thePrefix, const char * theFilename, int theFileFd,
              const char * theFormat, const char ** printTable,
              unsigned long long * bytesDoneP, unsigned long long * rowsP) {
  printDebug("doCheck starting:");
  // 
  // Initial state:
  *bytesDoneP = 0;
  *rowsP      = 0;
  int oddBytes = 0;
  //
  // Loop once per line from the file:
  for (;;) {
    // Loop once per big buffer (or part thereof):
    // Use bigBuf for the file being read.
    ssize_t got = read( theFileFd, bigBuf + oddBytes, bigBufSize - oddBytes);
    if (0 == got) {
      // The man page says this means EOF.
      printDebug("Hit end of input file.");
      if (0 != oddBytes) {
        // There were odd bytes from the previous read() call.
        snprintf( tempBuf, tempBufSize,
                  "Hit EOF with %d odd bytes remaining.", oddBytes);
        printError(tempBuf);
        // That function does not return.
      }
      // That's all, folks.
      return;
    }
    const char * bigP  = bigBuf;
    const char * pastP = bigBuf + got;
    while (lineLength <= (pastP - bigP)) {
      // Loop once per line within the big buffer:
      // Use lineBuf for expected:
      makeLine( lineBuf, *bytesDoneP, theFormat);
      if (0 != memcmp( bigP, lineBuf, lineLength)) {
        // Detected a mismatch:
        snprintf( tempBuf, tempBufSize,
                  "Mismatch at offset %llu, 0x%llx\n",
                  *bytesDoneP, *bytesDoneP);
        printf(tempBuf);
        printOne( "exp", lineBuf, printTable);
        printOne( "got", bigP   , printTable);
        *rowsP += 1;
      }
      bigP        += lineLength;
      *bytesDoneP += lineLength;
    }
    if (0 < (oddBytes = (pastP > bigP))) {
      // Got some odd bytes, so copy them to the start of the buffer.
      memcpy( bigBuf, bigP, oddBytes);
    }
  }
  snprintf( tempBuf, tempBufSize, "doCheck finished: %llu", *bytesDoneP);
  // *bytesDoneP and *rowsP have been kept updated.
  return;
}

//////////////////////////////////////////////////////////////////////////////

// Return current time in seconds as a double.

double currentTime() {
  printDebug("currentTime starting:");
  struct timeval timeBuf;
  if (0 != gettimeofday( &timeBuf, NULL)) {
    printError("gettimeofday() failed.");
    // That function does not return.
  }
  double result =    (double)timeBuf.tv_sec
                  + ((double)timeBuf.tv_usec / 1000.0 / 1000.0);
  snprintf( tempBuf, tempBufSize, "currentTime finished: %.2f", result);
  printDebug(tempBuf);
  return(result);
}

//////////////////////////////////////////////////////////////////////////////

void doWork( const char * theMode, const char * thePrefix,
             const char * theFilename, int theFileFd,
             const char ** printTable) {
  printDebug("doWork starting:");
  makeFormat(thePrefix); // puts result in static buffer formatBuf
  double startTime = currentTime();
  unsigned long long bytesDone;
  unsigned long long rowsPrinted;
  if (0 == strcmp( modeWrite, theMode)) {
    bytesDone = doWrite( theFilename, theFileFd,
                         formatBuf, printTable);
  } else {
    doCheck( thePrefix, theFilename, theFileFd,
             formatBuf, printTable,
             &bytesDone, &rowsPrinted);
  }
  //
  // Prepare to print summary statistics:
  float miBytes = (double)bytesDone / 1024.0 / 1024.0;
  float giBytes = miBytes / 1024.0;
  double endTime = currentTime();
  double elapsed = endTime - startTime;
  float miBps = miBytes / elapsed;
  //
  // Print the stats:
  printInfo("");
  snprintf( tempBuf, tempBufSize, "           File: %s",   theFilename);
  printInfo(tempBuf);
  snprintf( tempBuf, tempBufSize, "Bytes processed: %llu", bytesDone);
  printInfo(tempBuf);
  snprintf( tempBuf, tempBufSize, "  MiB processed: %.2f", miBytes);
  printInfo(tempBuf);
  snprintf( tempBuf, tempBufSize, "  GiB processed: %.2f", giBytes);
  printInfo(tempBuf);
  snprintf( tempBuf, tempBufSize, "   Rows printed: %llu", rowsPrinted);
  printInfo(tempBuf);
  snprintf( tempBuf, tempBufSize, "   elapsed secs: %.2f", elapsed);
  printInfo(tempBuf);
  snprintf( tempBuf, tempBufSize, " MiB per second: %.2f", miBps);
  printInfo(tempBuf);
  printInfo("");
  printDebug("doWork finished.");
}

//////////////////////////////////////////////////////////////////////////////

void closeFile(int theFileFd) {
  printDebug("closeFile starting:");
  if ((STDOUT_FILENO != theFileFd) && (STDIN_FILENO != theFileFd)) {
    // Doesn't matter here whether it is output or input:
    close(theFileFd);
  }
  printDebug("closeFile finished.");
}

//////////////////////////////////////////////////////////////////////////////

// Start here:
int main( int argc, const char ** argv) {
  checkAssumptions();
  const char * mode;
  const char * prefix;
  const char * filename;
  int          fileFd;
  getArgs( argc, argv, &mode, &prefix, &filename, &fileFd);
  doWork( mode, prefix, filename, fileFd, makePrintTable());
  closeFile(fileFd);
}

//////////////////////////////////////////////////////////////////////////////

// The End.
