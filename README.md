# checkunique

Write and check a stream of non-repeating data to verify block device capacity
and integrity.

The Golang version is too slow, most likely due to a combination of string
memory allocation and small-block file I/O.

The C version is intended to be faster.
